counter = 0
code = 0
imgArray = 0
previous = 0

firstRun = ->
  # $("#loveIsImg").fadeOut()
  $.ajax
    type: "GET"
    url: "./php/script.php"
    success: (data) ->
      data = jQuery.parseJSON(data)
      $("#loveIsImg").fadeIn()
      imgArray = data


#cacheImages(imgArray);
cacheImages = (images) ->
  images.forEach (imgPath) ->
    img = document.createElement("img")
    img.setAttribute "src", imgPath
    img.setAttribute "class", "hidden"
    document.body.appendChild img

getRandomImg = ->
  counter++
  if counter is 2
    setTimeout (->
      $("#backButton").css "visibility", "visible"
      $("#backButton").hide()
      $("#backButton").fadeIn()
    ), 500
  previous = code
  randImg = Math.floor(Math.random() * (imgArray.length))
  imgPath = imgArray[randImg].trim()
  imgPath = imgPath.replace(RegExp(" ", "g"), "%20")
  imgPath = imgPath.replace(/\\/g, "/")
  code = "<img id='tty' src=" + imgPath + "></img>"
  
  #console.log(code); // DEBUG OPTION
  $('#loveIsImg').fadeOut()
  setTimeout (->
    $('#loveIsImg').html code
    $("#tty").load ->
      $("#loveIsImg").fadeIn()
  ), 370
  
  #console.log("cached"); // DEBUG OPTION


previousImage = ->
  $("#loveIsImg").fadeOut()
  setTimeout (->
    $("#loveIsImg").html previous
  ), 370
  code = previous
  #console.log("cached"); // DEBUG OPTION
  $("#loveIsImg").fadeIn()
  $("#backButton").fadeOut()
  setTimeout (->
    $("#backButton").css "visibility", "hidden"
  ), 370
  $("#backButton").fadeIn()
  counter = 1
checkCached = (src) ->
  
  #console.log(src); // DEBUG OPTION
  image = new Image()
  image.src = src
  image.complete
$(document).ready ->
  $("#welcomeImage").hide()
  firstRun()

$(window).load ->
  $("#welcomeImage").fadeIn 2000
  $("#loveIsButton").click ->
    getRandomImg()

  $("#backButton").click ->
    previousImage()