<?php

$rootpath = './img/love_is';
$imgNames = array();
$fileinfos = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootpath)
);
foreach($fileinfos as $pathname => $fileinfo) {
    if (!$fileinfo->isFile()) continue;
    array_push($imgNames, $pathname);
}

echo implode('<br>', $imgNames);

?>