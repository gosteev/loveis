(function() {
  var cacheImages, checkCached, code, counter, firstRun, getRandomImg, imgArray, previous, previousImage;

  counter = 0;

  code = 0;

  imgArray = 0;

  previous = 0;

  firstRun = function() {
    return $.ajax({
      type: "GET",
      url: "./php/script.php",
      success: function(data) {
        data = jQuery.parseJSON(data);
        $("#loveIsImg").fadeIn();
        return imgArray = data;
      }
    });
  };

  cacheImages = function(images) {
    return images.forEach(function(imgPath) {
      var img;
      img = document.createElement("img");
      img.setAttribute("src", imgPath);
      img.setAttribute("class", "hidden");
      return document.body.appendChild(img);
    });
  };

  getRandomImg = function() {
    var imgPath, randImg;
    counter++;
    if (counter === 2) {
      setTimeout((function() {
        $("#backButton").css("visibility", "visible");
        $("#backButton").hide();
        return $("#backButton").fadeIn();
      }), 500);
    }
    previous = code;
    randImg = Math.floor(Math.random() * imgArray.length);
    imgPath = imgArray[randImg].trim();
    imgPath = imgPath.replace(RegExp(" ", "g"), "%20");
    imgPath = imgPath.replace(/\\/g, "/");
    code = "<img id='tty' src=" + imgPath + "></img>";
    $('#loveIsImg').fadeOut();
    return setTimeout((function() {
      $('#loveIsImg').html(code);
      return $("#tty").load(function() {
        return $("#loveIsImg").fadeIn();
      });
    }), 370);
  };

  previousImage = function() {
    $("#loveIsImg").fadeOut();
    setTimeout((function() {
      return $("#loveIsImg").html(previous);
    }), 370);
    code = previous;
    $("#loveIsImg").fadeIn();
    $("#backButton").fadeOut();
    setTimeout((function() {
      return $("#backButton").css("visibility", "hidden");
    }), 370);
    $("#backButton").fadeIn();
    return counter = 1;
  };

  checkCached = function(src) {
    var image;
    image = new Image();
    image.src = src;
    return image.complete;
  };

  $(document).ready(function() {
    $("#welcomeImage").hide();
    return firstRun();
  });

  $(window).load(function() {
    $("#welcomeImage").fadeIn(2000);
    $("#loveIsButton").click(function() {
      return getRandomImg();
    });
    return $("#backButton").click(function() {
      return previousImage();
    });
  });

}).call(this);

